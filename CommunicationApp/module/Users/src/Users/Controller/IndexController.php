<?php
namespace Users\Controller;

use Zend\Mvc\Controller\AbstractActionController;

class IndexController extends AbstractActionController
{
	protected $dbAdapter = null;

	public function listAction()
	{

		$results = [];
		$cities = $this->getCities();

		$filters = empty($_POST['filter']) ? [] : json_decode($_POST['filter'], true);

		$dbAdapter = $this->getMyAdapter();

		$sql
			= "SELECT `user`.`id`, `user`.`name`, `education`.`id` as education_id, `education`.`label`
				FROM  `user` INNER JOIN  `education` ON `user`.`id` = `education`.`user_id` WHERE `user`.`name` IS NOT NULL";

		foreach ($filters as $key => $filter) {
			$value = $filter['value'];
			$field = $filter['field'];

			if ($field == 'name') {
				$value = trim(mysql_real_escape_string($value));
				$field = "`user`.`$field`";
				$sql .= " AND ";
				$sql .= "$field LIKE '$value%'";
			} elseif ($field == 'label') {
				$field = "`education`.`id`";
				$arr_values = array_filter($value, function ($var) {
					return (int)$var;
				});
				$str_values = implode(',', $arr_values);
				$sql .= " AND ";
				$sql .= "$field IN ($str_values)";
			} else {
				$arr_values = array_filter($value, function ($var) {
					return (int)$var;
				});
				$cities_filtered = $this->getCitiesByIds($cities, $arr_values);
			}
		}
		$cities = empty($cities_filtered) ? $cities : $cities_filtered;
		$statement = $dbAdapter->query($sql);

		/* @var $results ZendDbResultSetResultSet */
		$users = $statement->execute();
		$i = 0;
		foreach ($users as $key => $user) {
			$cities_by_user_id = $this->gitCitiesByUserId($cities, (int)$user['id']);
			if (!empty($cities_by_user_id)) {
				$results[$i] = $user;
				$results[$i]['cities'] = $cities_by_user_id;
				++$i;
			}
		}
		header('Content-Type: application/json');
		echo json_encode($results);
		die;

	}

	public function updateAction()
	{

		$response = ['success' => false];

		if (!empty($_POST['id']) && !empty($_POST['label'])) {

			$user_id = (int)$_POST['id'];
			$label = trim($_POST['label']);

			$dbAdapter = $this->getMyAdapter();

			$qi = function ($name) use ($dbAdapter) {
				return $dbAdapter->platform->quoteIdentifier($name);
			};
			$fp = function ($name) use ($dbAdapter) {
				return $dbAdapter->driver->formatParameterName($name);
			};

			$sql = 'UPDATE ' . $qi('education')
				. ' SET ' . $qi('label') . ' = ' . $fp('label')
				. ' WHERE ' . $qi('user_id') . ' = ' . $fp('user_id');

			$parameters = array(
				'label' => $label,
				'user_id' => $user_id
			);

			$sm = $dbAdapter->query($sql);
			$sm->execute($parameters);

			$response = ['success' => true, 'message' => 'Обновлено!'];
		}

		header('Content-Type: application/json');
		echo json_encode($response);
		die;
	}

	public function educationAction()
	{
		$sql = "SELECT * FROM  `education` GROUP BY `label`";
		$statement = $this->getMyAdapter()->query($sql);
		$result_obj = $statement->execute();
		$education = [];
		foreach ($result_obj as $result) {
			$education[] = $result;
		}
		header('Content-Type: application/json');
		echo json_encode($education);
		die;

	}

	public function citiesAction()
	{
		$cities = $this->getCities(null, true);
		header('Content-Type: application/json');
		echo json_encode($cities);
		die;
	}

	protected function getCities($ids = null, $group_by = false)
	{
		$sql = "SELECT * FROM `cities`";

		if (!empty($ids)) {
			$ids = implode(',', $ids);
			$sql .= " WHERE `id` IN ($ids)";
		}
		if ($group_by) {
			$sql .= " GROUP BY `city`";
		}

		$statement = $this->getMyAdapter()->query($sql);
		$result_obj = $statement->execute();
		$cities = [];
		foreach ($result_obj as $result) {
			$cities[] = $result;
		}

		return (array)$cities;
	}

	protected function getCitiesByIds(array $cities, array $ids)
	{
		$new_cities = [];
		foreach ($cities as $city) {
			if (in_array($city['id'], $ids)) {
				$new_cities[] = $city;
			}
		}
		return $new_cities;
	}

	protected function gitCitiesByUserId(array $cities, $user_id)
	{
		$cities_by_user_id = [];
		foreach ($cities as $city) {
			if ($city['user_id'] == $user_id) {
				$cities_by_user_id[] = $city['city'];
			}
		}
		return implode(',', $cities_by_user_id);
	}

	protected function getMyAdapter()
	{
		if (empty($this->dbAdapter)) {
			$sm = $this->getServiceLocator();
			$this->dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
		}
		return $this->dbAdapter;
	}
}