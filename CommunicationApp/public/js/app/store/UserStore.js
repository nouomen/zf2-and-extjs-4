Ext.define('UserApp.store.UserStore', {
	extend: 'Ext.data.Store',
	model: 'UserApp.model.User',
	autoLoad: true,
	remoteSort: true,
	storeId: 'UserStore',
	proxy: {
		type: 'ajax',
		url: '/users/index/list',
		actionMethods: {
			read: 'POST'
		},
		reader: {
			type: 'json',
			root: 'users',
			successProperty: 'success'
		}
	}
});