Ext.define('UserApp.controller.Users', {

	extend: 'Ext.app.Controller',
	views: ['UserList', 'User'],
	stores: ['UserStore'],
	models: ['User', 'Education', 'City'],

	init: function ()
	{
		this.control({
			'viewport > userList': {
				itemdblclick: this.editUser,
				selectionchange: this.selectUser
			},
			'userwindow button[action=clear]': {
				click: this.clearForm
			},
			'userwindow button[action=save]': {
				click: this.saveUser
			}
		});
	},

	selectUser: function (selModel, selections)
	{
		console.log('select');
		var delBut = Ext.ComponentQuery.query('#deleteUser')[0];
	},
	saveUser: function (button)
	{
		console.log('save');
		var win = button.up('window'),
			form = win.down('form'),
			values = form.getValues();

		Ext.Ajax.request({
			url: '/users/index/update',
			params: values,
			success: function (response)
			{
				win.close();
				var data = Ext.decode(response.responseText);
				if (data.success) {
					console.log(data);
					var store = Ext.widget('userList').getStore();
					store.load();
					Ext.Msg.alert('Обновление', data.message);
				}
				else {
					Ext.Msg.alert('Обновление', 'Не удалось обновить книгу в библиотеке');
				}
			}
		});
	},

	clearForm: function (grid, record)
	{
		var view = Ext.widget('userwindow');
		view.down('form').getForm().reset();
	},

	editUser: function (grid, record)
	{
		console.log('edit');
		var view = Ext.widget('userwindow');
		view.down('form').loadRecord(record);
	}
});