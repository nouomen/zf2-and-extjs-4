Ext.Loader.setPath('Ext.ux', '/js/ux');
Ext.require([
	'Ext.ux.grid.FiltersFeature',
]);

var filters = {
	ftype: 'filters',
	encode: true,
	local: false
};

Ext.define('UserApp.view.UserList', {
	extend: 'Ext.grid.Panel',
	alias: 'widget.userList',
	id: 'userID',
	title: 'Список пользователей',
	store: 'UserStore',
	features: [filters],

	initComponent: function ()
	{
		this.columns = [
			{
				header: 'ID',
				dataIndex: 'id',
				flex: 1
			},
			{
				header: 'Имя',
				dataIndex: 'name',
				flex: 1,
				filter: {
					type: 'string'
				}
			},
			{
				header: 'Образование',
				dataIndex: 'label',
				flex: 1,
				filter: {
					type: 'list',
					labelField: 'label',
					store: Ext.create('Ext.data.Store', {
						model: 'UserApp.model.Education',
						proxy: {
							type: 'ajax',
							url: '/users/index/education',
							reader: {
								type: 'json',
								root: 'data',
								totalProperty: 'totalCount'
							}
						}
					})
				}
			},
			{
				header: 'Города',
				dataIndex: 'cities',
				flex: 1,
				filter: {
					type: 'list',
					labelField: 'city',
					store: Ext.create('Ext.data.Store', {
						model: 'UserApp.model.City',
						proxy: {
							type: 'ajax',
							url: '/users/index/cities',
							reader: {
								type: 'json',
								root: 'data',
								totalProperty: 'totalCount'
							}
						}
					})
				}
			}
		];

		this.callParent(arguments);
	}
});