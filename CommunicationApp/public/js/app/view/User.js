Ext.define('UserApp.view.User', {
	extend: 'Ext.window.Window',
	alias: 'widget.userwindow',
	title: 'Пользователь',
	layout: 'fit',
	autoShow: true,

	initComponent: function () {
		this.items = [
			{
				xtype: 'form',
				items: [
					{
						xtype: 'textfield',
						name: 'label',
						fieldLabel: 'Образование'
					},
					{
						name: 'id',
						fieldLabel: 'id',
						minValue: 1,
						xtype: 'hidden'
					},
				]
			}
		];
		this.dockedItems = [{
			xtype: 'toolbar',
			docked: 'top',
		}];
		this.buttons = [
			{
				text: 'Clear',
				scope: this,
				action: 'clear'
			},
			{
				text: 'Save',
				action: 'save'
			}
		];

		this.callParent(arguments);
	}
});