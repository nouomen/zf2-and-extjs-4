Ext.define('UserApp.model.User', {
	extend: 'Ext.data.Model',
	fields: [
		'id',
		'name',
		'label',
		'cities'
	]
});
