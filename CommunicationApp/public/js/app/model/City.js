Ext.define('UserApp.model.City', {
	extend: 'Ext.data.Model',
	fields: [
		'id',
		'city',
	]
});
