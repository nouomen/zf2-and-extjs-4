Ext.define('UserApp.model.Education', {
	extend: 'Ext.data.Model',
	fields: [
		'id',
		'label',
	]
});
